# -*- coding: utf-8 -*-
from __future__ import unicode_literals

#from pyexcel_xls import get_data
import pyexcel as pe
import json

from django.shortcuts import render
from django.views.generic import View
from django.http import HttpResponse


#def main_page(request):
#	return render(request, 'main.html')

class ViewMainPage(View):
	template_name = 'main.html'

	def get(self, request):
		return render(request, self.template_name)

	def post(self, request):
		file_exist = request.FILES.get('upload_excel_file', None)
		if file_exist:
			if not file_exist.name.endswith('.xls'):
				return HttpResponse('NO TYPE')

			records = pe.get_array(file_type='xls', file_content=file_exist.read(), start_row=8)
			return HttpResponse(json.dumps(records), content_type='application/json')
		return HttpResponse('NO')